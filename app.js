const getPokemonUrl = (id) => `https://pokeapi.co/api/v2/pokemon/${id}`;

const generatePokemonPromises = () => Array(150).fill().map((_, index) =>
    fetch(getPokemonUrl(index + 1)).then((response) => response.json())
);

const convertPokemonId = (_id) => {
    if (_id < 10) {
        return "00" + _id;
    }
    if ((_id >= 10) & (_id <= 99)) {
        return "0" + _id;
    } else {
        return _id;
    }
};

const generateHTML = (pokemons) => pokemons.reduce((accumulator, { name, id, types }) => {
    const elementTypes = types.map((typeInfo) => typeInfo.type.name);
    accumulator += `
        <li class="card ${elementTypes[0]}">
            <img class="card-image" alt="${name}"
            src="https://assets.pokemon.com/assets/cms2/img/pokedex/full/${convertPokemonId(id)}.png"/>
        <h2 class="card-title">${id}. ${name}</h2>
        <p class="card-subtitle">${elementTypes.join(" | ")}</p>
        </li>`;
    return accumulator;
}, "");

const insertPokemonsIntoPage = (pokemons) => {
    const ul = document.querySelector('[data-js="pokedex"]');
    ul.innerHTML = pokemons;
};

const prokemonPromises = generatePokemonPromises();

Promise.all(prokemonPromises)
    .then(generateHTML)
    .then(insertPokemonsIntoPage);

